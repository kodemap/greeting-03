/**
 * Say goodbye
 */
function sayGoodbye () {
    console.log('Goodbye');
}

/**
 * Say goodbye to person
 * @param person
 */
function sayGoodbyePerson (person) {
    console.log('Goodbye ' + person);
}