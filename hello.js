/**
 * Say hello
 */
function sayHello () {
  console.log('Hello');
}

/**
 * Say hello to person
 * @param person
 */
function sayHelloPerson (person) {
  console.log('Hello ' + person);
}
